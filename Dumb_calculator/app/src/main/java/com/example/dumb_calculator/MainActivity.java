package com.example.dumb_calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text1 = findViewById(R.id.textView1);
        but1 = findViewById(R.id.button1);
        but2 = findViewById(R.id.button2);
        but3 = findViewById(R.id.button3);
        but4 = findViewById(R.id.button4);
        but5 = findViewById(R.id.button5);
        but6 = findViewById(R.id.button6);
        but7 = findViewById(R.id.button7);
        but8 = findViewById(R.id.button8);
        but9 = findViewById(R.id.button9);
        but10 = findViewById(R.id.button10);
        but11 = findViewById(R.id.button11);
        but12 = findViewById(R.id.button12);
        but13 = findViewById(R.id.button13);
        but14 = findViewById(R.id.button14);
        but15 = findViewById(R.id.button15);
        but16 = findViewById(R.id.button16);
    }
    Button but1;
    Button but2;
    Button but3;
    Button but4;
    Button but5;
    Button but6;
    Button but7;
    Button but8;
    Button but9;
    Button but10;
    Button but11;
    Button but12;
    Button but13;
    Button but14;
    Button but15;
    Button but16;
    TextView text1;
    double i;
    String znak;
    String s;
    String news;
    public void click1(View view)
    {

            if (text1.getText().equals("0") || text1.getText().toString().equals("Infinity") || text1.getText().toString().equals("NaN") || text1.getText().toString().equals("Ошибка"))
            {
                text1.setText("1");
            }
            else
                {
                text1.setText(text1.getText() + "1");
                }
    }

    public void click2(View view) {
        if (text1.getText().equals("0")|| text1.getText().toString().equals("NaN") || text1.getText().toString().equals("Infinity")|| text1.getText().toString().equals("Ошибка")) {
            text1.setText("2");
        } else
        {
            text1.setText(text1.getText() + "2");
        }
    }

    public void click3(View view) {
        if (text1.getText().equals("0")|| text1.getText().toString().equals("NaN")|| text1.getText().toString().equals("Infinity")|| text1.getText().toString().equals("Ошибка")) {
            text1.setText("3");
        } else
        {
            text1.setText(text1.getText() + "3");
        }
    }

    public void clickPlus(View view) {
        i = Double.parseDouble(text1.getText().toString());
        text1.setText("0");
        znak = "+";
    }

    public void clickResult(View view) {

        if (text1.getText()==""){
            text1.setText("Ошибка");
        }
        else {
        switch (znak)
        {
            case "+": text1.setText(String.valueOf(i + Double.parseDouble(text1.getText().toString())));
            break;
            case "-": text1.setText(String.valueOf(i - Double.parseDouble(text1.getText().toString())));
            break;
            case "*": text1.setText(String.valueOf(i * Double.parseDouble(text1.getText().toString())));
            break;
            case "/": text1.setText(String.valueOf(i / Double.parseDouble(text1.getText().toString())));
            break;
            }
        }
            }
    public void click4(View view) {
        if (text1.getText().equals("0")|| text1.getText().toString().equals("NaN")|| text1.getText().toString().equals("Infinity") || text1.getText().toString().equals("Ошибка")) {
            text1.setText("4");
        } else
        {
            text1.setText(text1.getText() + "4");
        }
    }


    public void click5(View view) {
        if (text1.getText().equals("0")|| text1.getText().toString().equals("Infinity")|| text1.getText().toString().equals("NaN")|| text1.getText().toString().equals("Ошибка")) {
            text1.setText("5");
        } else
        {
            text1.setText(text1.getText() + "5");
        }
    }

    public void click6(View view) {
        if (text1.getText().equals("0")|| text1.getText().toString().equals("NaN")|| text1.getText().toString().equals("Infinity")|| text1.getText().toString().equals("Ошибка")) {
            text1.setText("6");
        } else
        {
            text1.setText(text1.getText() + "6");
        }
    }

    public void clickMinus(View view) {
        i = Double.parseDouble(text1.getText().toString());
        text1.setText("0");
        znak = "-";
    }

    public void click7(View view) {
        if (text1.getText().equals("0")|| text1.getText().toString().equals("NaN")|| text1.getText().toString().equals("Infinity")|| text1.getText().toString().equals("Ошибка")) {
            text1.setText("7");
        } else
        {
            text1.setText(text1.getText() + "7");
        }
    }

    public void click8(View view) {
        if (text1.getText().equals("0")|| text1.getText().toString().equals("NaN")|| text1.getText().toString().equals("Infinity")|| text1.getText().toString().equals("Ошибка")) {
            text1.setText("8");
        } else
        {
            text1.setText(text1.getText() + "8");
        }
    }

    public void click9(View view) {
        if (text1.getText().equals("0")|| text1.getText().toString().equals("NaN")|| text1.getText().toString().equals("Infinity")|| text1.getText().toString().equals("Ошибка")) {
            text1.setText("9");
        } else
        {
            text1.setText(text1.getText() + "9");
        }
    }

    public void clickProiz(View view) {
        i = Double.parseDouble(text1.getText().toString());
        text1.setText("0");
        znak = "*";
    }

    public void click0(View view) {
        if (text1.getText().equals("0")|| text1.getText().toString().equals("NaN")|| text1.getText().toString().equals("Infinity")|| text1.getText().toString().equals("Ошибка")) {
            text1.setText("0");
        } else
        {
            text1.setText(text1.getText() + "0");
        }
    }

    public void clickZapitya(View view) {
        if (text1.getText().toString().contains(".")) {
            text1.setText(text1.getText() + "");
        } else
        {
            text1.setText(text1.getText() + ".");
        }
    }

    public void clickDel(View view) {
        i = Double.parseDouble(text1.getText().toString());
        text1.setText("0");
        znak = "/";
    }

    public void clickAC(View view) {
        text1.setText("0");
    }

    public void clickBack(View view) {

        s = text1.getText().toString();
        news = text1.getText().toString();
        if(s!="")
        {
            if(s.length()==1)
            {
                text1.setText("");
            }
            else
            {
                news="";
                news+=s.substring(0,(s.length()-1));
                text1.setText(news);
            }
        }
    }
    private void getScreenOrientation(){
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            setTitle("Портретная ориентация");
        }
        else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setTitle("Альбомная ориентация");
        }
    }
}
